<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="subPripoistenia" language="groovy" pageWidth="475" pageHeight="842" columnWidth="475" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="b432aa20-01b8-44ad-8288-f9104171fbed">
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Base" isDefault="true" hAlign="Left" vAlign="Top" fontName="Verdana" fontSize="9" pdfFontName="Verdana" pdfEncoding="UTF8" isPdfEmbedded="false"/>
	<parameter name="DOC_ID" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="EXECUTION_ID" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT LINK_PRSN, SNAME, COVERAGE, SPOISUMA, NPOISUMA, NOVEPOI, LISTAGG(DD, ', ') WITHIN GROUP (ORDER BY SNAME) AS String_meno FROM
(
select    np.name as sname,
          sp.lump_sum as spoisuma,
          np.lump_sum as npoisuma,
          np.prem_bgn as novepoi,
          decode (nk.title, 'No Title', '', nk.title) as titul,
          nk.frst_nm as meno,
          nk.nm_st as priezvisko,
          replace (nk.birth_td, '/', '.') as birthdate,
          case
            when regexp_like (sp.name, '^(P_)+')
            then
                 'Tútor '
              || substr (nk.sort_ordr, -1)
              || ': '
              || trim (
                      decode (nk.title, 'No Title', '', nk.title)
                   || ' '
                   || nk.frst_nm
                   || ' '
                   || nk.nm_st
                   || CASE WHEN (nk.title2 is null or nk.title2 = 'No Title') THEN '' ELSE ', ' || nk.TITLE2 END
                   || ' ('
                   || replace (nk.birth_td, '/', '.')
                   || ')')
            else
                 'Poistený '
              || substr (nk.sort_ordr, -1)
              || ': '
              || trim (
                      decode (nk.title, 'No Title', '', nk.title)
                   || ' '
                   || nk.frst_nm
                   || ' '
                   || nk.nm_st
	         || CASE WHEN (nk.title2 is null or nk.title2 = 'No Title') THEN '' ELSE ', ' || nk.TITLE2 END
                   || ' ('
                   || replace (nk.birth_td, '/', '.')
                   || ')')
          end   as dd,
          nk.birth_id as birth_id,
          nk.rgstrtn_nmbr as rgstrtn_nmbr,
          nk.l_pm_risk_cvrg_ls as link_prsn,
          tc.name as coverage
from      printmanager.pm_risk_cvrg_ls np
left join printmanager.pm_risk_cvrg_ls sp on sp.doc_id = np.doc_id and sp.execution_id = np.execution_id and sp.name = np.name and sp.prem_bgn is null
join      printmanager.pm_prsn_dtl nk on nk.l_pm_risk_cvrg_ls = np.o__num
left join printmanager.pm_prsn_dtl sk on sk.l_pm_risk_cvrg_ls = sp.o__num
join      printmanager.tech_list_cvrg tc on tc.code = np.name
where     np.prem_bgn is not null
  and     nk.birth_id = nvl(sk.birth_id, nk.birth_id)
  and     tc.main = 'N'
  and     np.doc_id = $P{DOC_ID}
  and     np.execution_id = $P{EXECUTION_ID}
)
group by LINK_PRSN, SNAME, COVERAGE, SPOISUMA, NPOISUMA, NOVEPOI order by string_meno]]>
	</queryString>
	<field name="LINK_PRSN" class="java.lang.String"/>
	<field name="SNAME" class="java.lang.String"/>
	<field name="COVERAGE" class="java.lang.String"/>
	<field name="SPOISUMA" class="java.math.BigDecimal"/>
	<field name="NPOISUMA" class="java.math.BigDecimal"/>
	<field name="NOVEPOI" class="java.math.BigDecimal"/>
	<field name="STRING_MENO" class="java.lang.String"/>
	<group name="StringName" isReprintHeaderOnEachPage="true" minHeightToStartNewPage="55" keepTogether="true">
		<groupExpression><![CDATA[$F{STRING_MENO}]]></groupExpression>
		<groupHeader>
			<band height="27">
				<staticText>
					<reportElement uuid="e2d840a5-1ed9-4c4d-b9c9-b573af78918b" positionType="Float" x="0" y="18" width="210" height="9"/>
					<box>
						<pen lineWidth="0.25" lineColor="#999999"/>
						<topPen lineWidth="0.25" lineColor="#999999"/>
						<leftPen lineWidth="0.25" lineColor="#999999"/>
						<bottomPen lineWidth="0.25" lineColor="#999999"/>
						<rightPen lineWidth="0.25" lineColor="#999999"/>
					</box>
					<textElement>
						<font fontName="Verdana" size="7" isBold="true"/>
						<paragraph leftIndent="2"/>
					</textElement>
					<text><![CDATA[Názov pripoistenia]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="536d7743-95bf-406f-8579-3cd8d26606f9" positionType="Float" x="210" y="18" width="95" height="9"/>
					<box>
						<pen lineWidth="0.25" lineColor="#999999"/>
						<topPen lineWidth="0.25" lineColor="#999999"/>
						<leftPen lineWidth="0.25" lineColor="#999999"/>
						<bottomPen lineWidth="0.25" lineColor="#999999"/>
						<rightPen lineWidth="0.25" lineColor="#999999"/>
					</box>
					<textElement>
						<font fontName="Verdana" size="7" isBold="true"/>
						<paragraph leftIndent="2"/>
					</textElement>
					<text><![CDATA[Aktuálna poistná suma]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="37b2f43e-2d59-462d-9c61-7664778a42f9" positionType="Float" x="305" y="18" width="115" height="9"/>
					<box>
						<pen lineWidth="0.25" lineColor="#999999"/>
						<topPen lineWidth="0.25" lineColor="#999999"/>
						<leftPen lineWidth="0.25" lineColor="#999999"/>
						<bottomPen lineWidth="0.25" lineColor="#999999"/>
						<rightPen lineWidth="0.25" lineColor="#999999"/>
					</box>
					<textElement>
						<font fontName="Verdana" size="7" isBold="true"/>
						<paragraph leftIndent="2"/>
					</textElement>
					<text><![CDATA[Zvýšená nová poistná suma]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="0bc541f4-079b-4ef6-894b-6dc068ef1de8" positionType="Float" x="420" y="18" width="55" height="9"/>
					<box>
						<pen lineWidth="0.25" lineColor="#999999"/>
						<topPen lineWidth="0.25" lineColor="#999999"/>
						<leftPen lineWidth="0.25" lineColor="#999999"/>
						<bottomPen lineWidth="0.25" lineColor="#999999"/>
						<rightPen lineWidth="0.25" lineColor="#999999"/>
					</box>
					<textElement>
						<font fontName="Verdana" size="7" isBold="true"/>
						<paragraph leftIndent="2"/>
					</textElement>
					<text><![CDATA[Nové poistné]]></text>
				</staticText>
				<textField>
					<reportElement uuid="86835564-f547-4696-8cc1-a4bdb2752186" x="0" y="9" width="475" height="9"/>
					<textElement>
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{STRING_MENO}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="22" splitType="Stretch">
			<staticText>
				<reportElement uuid="24c3d525-fb0f-41a1-8f64-a0c563a373df" x="0" y="11" width="100" height="11"/>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[PRIPOISTENIA]]></text>
			</staticText>
		</band>
	</title>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="10" splitType="Stretch">
			<textField isStretchWithOverflow="true">
				<reportElement uuid="1533bf87-3073-48a4-a13a-08e9aa1ef9d1" x="0" y="0" width="210" height="10"/>
				<box>
					<pen lineWidth="0.25" lineColor="#999999"/>
					<topPen lineWidth="0.25" lineColor="#999999"/>
					<leftPen lineWidth="0.25" lineColor="#999999"/>
					<bottomPen lineWidth="0.25" lineColor="#999999"/>
					<rightPen lineWidth="0.25" lineColor="#999999"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font fontName="Verdana" size="7"/>
					<paragraph leftIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{COVERAGE}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="97fb07df-c468-41be-bd69-58f1d85ac90d" stretchType="RelativeToTallestObject" x="210" y="0" width="95" height="10"/>
				<box>
					<pen lineWidth="0.25" lineColor="#999999"/>
					<topPen lineWidth="0.25" lineColor="#999999"/>
					<leftPen lineWidth="0.25" lineColor="#999999"/>
					<bottomPen lineWidth="0.25" lineColor="#999999"/>
					<rightPen lineWidth="0.25" lineColor="#999999"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Verdana" size="7"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{SPOISUMA} == 0 || $F{SPOISUMA} == null) ? "------" : new DecimalFormat("#,##0.00").format($F{SPOISUMA}) + " €"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="6a423b59-f3a4-4b80-8aea-0af81ccc44ef" stretchType="RelativeToTallestObject" x="305" y="0" width="115" height="10"/>
				<box>
					<pen lineWidth="0.25" lineColor="#999999"/>
					<topPen lineWidth="0.25" lineColor="#999999"/>
					<leftPen lineWidth="0.25" lineColor="#999999"/>
					<bottomPen lineWidth="0.25" lineColor="#999999"/>
					<rightPen lineWidth="0.25" lineColor="#999999"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Verdana" size="7"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{NPOISUMA} == 0 ? "------" : new DecimalFormat("#,##0.00").format($F{NPOISUMA}) + " €"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="760ba5d2-6e69-4e11-a925-9a9788b72604" stretchType="RelativeToTallestObject" x="420" y="0" width="55" height="10"/>
				<box>
					<pen lineWidth="0.25" lineColor="#999999"/>
					<topPen lineWidth="0.25" lineColor="#999999"/>
					<leftPen lineWidth="0.25" lineColor="#999999"/>
					<bottomPen lineWidth="0.25" lineColor="#999999"/>
					<rightPen lineWidth="0.25" lineColor="#999999"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Verdana" size="7"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[new DecimalFormat("#,##0.00").format($F{NOVEPOI}) + " €"]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
