<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="subMainCvrg" language="groovy" pageWidth="475" pageHeight="842" columnWidth="475" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="b432aa20-01b8-44ad-8288-f9104171fbed">
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Base" isDefault="true" hAlign="Left" vAlign="Top" fontName="Verdana" fontSize="9" pdfFontName="Verdana" pdfEncoding="UTF8" isPdfEmbedded="false"/>
	<parameter name="DOC_ID" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="EXECUTION_ID" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="PRODUCT" class="java.lang.String" isForPrompting="false"/>
	<queryString>
		<![CDATA[SELECT LINK_PRSN,
  SNAME,
  COVERAGE,
  SPOISUMA,
  NPOISUMA,
  NOVEPOI,
  LISTAGG(DD, ', ') WITHIN GROUP (
ORDER BY SNAME) AS String_meno
FROM
  (SELECT np.name                               AS sname,
    sp.lump_sum                                 AS spoisuma,
    np.lump_sum                                 AS npoisuma,
    np.prem_bgn                                 AS novepoi,
    DECODE (nk.title, 'No Title', '', nk.title) AS titul,
    nk.frst_nm                                  AS meno,
    nk.nm_st                                    AS priezvisko,
    REPLACE (nk.birth_td, '/', '.')             AS birthdate,
    'Poistený '
    || SUBSTR(nk.sort_ordr, -1)
    || ': '
    || trim(DECODE(nk.title, 'No Title', '', nk.title)
    || ' '
    || nk.frst_nm
    || ' '
    || nk.nm_st
    || CASE WHEN (nk.title2 is null or nk.title2 = 'No Title') THEN '' ELSE ', ' || nk.TITLE2 END
    || ' ('
    || REPLACE(nk.birth_td,'/','.')
    || ')' )             AS dd,
    nk.birth_id          AS birth_id,
    nk.rgstrtn_nmbr      AS rgstrtn_nmbr,
    nk.l_pm_risk_cvrg_ls AS link_prsn,
    tc.name              AS coverage
  FROM printmanager.pm_risk_cvrg_ls np
  LEFT JOIN printmanager.pm_risk_cvrg_ls sp
  ON sp.doc_id        = np.doc_id
  AND sp.execution_id = np.execution_id
  AND sp.name         = np.name
  AND sp.prem_bgn    IS NULL
  JOIN printmanager.pm_prsn_dtl nk
  ON nk.l_pm_risk_cvrg_ls = np.o__num
  LEFT JOIN printmanager.pm_prsn_dtl sk
  ON sk.l_pm_risk_cvrg_ls = sp.o__num
  JOIN printmanager.tech_list_cvrg tc
  ON tc.code          = np.name
  WHERE np.prem_bgn  IS NOT NULL
  AND nk.birth_id     = NVL(sk.birth_id, nk.birth_id)
  AND tc.main         = 'A'
  AND np.doc_id       = $P{DOC_ID}
  AND np.execution_id = $P{EXECUTION_ID}
  )
GROUP BY LINK_PRSN,
  SNAME,
  COVERAGE,
  SPOISUMA,
  NPOISUMA,
  NOVEPOI
ORDER BY string_meno]]>
	</queryString>
	<field name="LINK_PRSN" class="java.lang.String"/>
	<field name="SNAME" class="java.lang.String"/>
	<field name="COVERAGE" class="java.lang.String"/>
	<field name="SPOISUMA" class="java.math.BigDecimal"/>
	<field name="NPOISUMA" class="java.math.BigDecimal"/>
	<field name="NOVEPOI" class="java.math.BigDecimal"/>
	<field name="STRING_MENO" class="java.lang.String"/>
	<group name="String_meno" isReprintHeaderOnEachPage="true" minHeightToStartNewPage="55" keepTogether="true">
		<groupExpression><![CDATA[$F{STRING_MENO}]]></groupExpression>
		<groupHeader>
			<band height="27">
				<staticText>
					<reportElement uuid="e2d840a5-1ed9-4c4d-b9c9-b573af78918b" positionType="Float" x="0" y="18" width="210" height="9"/>
					<box>
						<pen lineWidth="0.25" lineColor="#999999"/>
						<topPen lineWidth="0.25" lineColor="#999999"/>
						<leftPen lineWidth="0.25" lineColor="#999999"/>
						<bottomPen lineWidth="0.25" lineColor="#999999"/>
						<rightPen lineWidth="0.25" lineColor="#999999"/>
					</box>
					<textElement>
						<font fontName="Verdana" size="7" isBold="true"/>
						<paragraph leftIndent="2"/>
					</textElement>
					<text><![CDATA[Názov poistenia]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="536d7743-95bf-406f-8579-3cd8d26606f9" positionType="Float" x="210" y="18" width="95" height="9"/>
					<box>
						<pen lineWidth="0.25" lineColor="#999999"/>
						<topPen lineWidth="0.25" lineColor="#999999"/>
						<leftPen lineWidth="0.25" lineColor="#999999"/>
						<bottomPen lineWidth="0.25" lineColor="#999999"/>
						<rightPen lineWidth="0.25" lineColor="#999999"/>
					</box>
					<textElement>
						<font fontName="Verdana" size="7" isBold="true"/>
						<paragraph leftIndent="2"/>
					</textElement>
					<text><![CDATA[Aktuálna poistná suma]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="37b2f43e-2d59-462d-9c61-7664778a42f9" positionType="Float" x="305" y="18" width="115" height="9"/>
					<box>
						<pen lineWidth="0.25" lineColor="#999999"/>
						<topPen lineWidth="0.25" lineColor="#999999"/>
						<leftPen lineWidth="0.25" lineColor="#999999"/>
						<bottomPen lineWidth="0.25" lineColor="#999999"/>
						<rightPen lineWidth="0.25" lineColor="#999999"/>
					</box>
					<textElement>
						<font fontName="Verdana" size="7" isBold="true"/>
						<paragraph leftIndent="2"/>
					</textElement>
					<text><![CDATA[Zvýšená nová poistná suma]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="0bc541f4-079b-4ef6-894b-6dc068ef1de8" positionType="Float" x="420" y="18" width="55" height="9"/>
					<box>
						<pen lineWidth="0.25" lineColor="#999999"/>
						<topPen lineWidth="0.25" lineColor="#999999"/>
						<leftPen lineWidth="0.25" lineColor="#999999"/>
						<bottomPen lineWidth="0.25" lineColor="#999999"/>
						<rightPen lineWidth="0.25" lineColor="#999999"/>
					</box>
					<textElement>
						<font fontName="Verdana" size="7" isBold="true"/>
						<paragraph leftIndent="2"/>
					</textElement>
					<text><![CDATA[Nové poistné]]></text>
				</staticText>
				<textField>
					<reportElement uuid="86835564-f547-4696-8cc1-a4bdb2752186" x="0" y="9" width="475" height="9"/>
					<textElement>
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{SNAME}.equals("RWD-W") ? $F{STRING_MENO}.replaceAll( "Poistený", "Tútor" ) : $F{STRING_MENO}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement uuid="9a09f11b-0dd0-4c72-ac7f-f6945ea5e366" positionType="Float" mode="Opaque" x="0" y="18" width="475" height="9">
						<printWhenExpression><![CDATA[$F{SNAME}.equals("RWD-W")]]></printWhenExpression>
					</reportElement>
					<textElement markup="none">
						<font fontName="Verdana" size="7"/>
					</textElement>
					<text><![CDATA[V prípade úmrtia tútora preberá poisťovňa povinnosť platiť poistné z Hlavného poistenia.]]></text>
				</staticText>
			</band>
		</groupHeader>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="11">
			<staticText>
				<reportElement uuid="24c3d525-fb0f-41a1-8f64-a0c563a373df" positionType="Float" x="0" y="0" width="135" height="11" isRemoveLineWhenBlank="true"/>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[HLAVNÉ POISTENIE]]></text>
			</staticText>
		</band>
	</title>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="10" splitType="Stretch">
			<printWhenExpression><![CDATA[!$F{SNAME}.equals("RWD-W")]]></printWhenExpression>
			<textField isStretchWithOverflow="true">
				<reportElement uuid="1533bf87-3073-48a4-a13a-08e9aa1ef9d1" x="0" y="0" width="210" height="10"/>
				<box>
					<pen lineWidth="0.25" lineColor="#999999"/>
					<topPen lineWidth="0.25" lineColor="#999999"/>
					<leftPen lineWidth="0.25" lineColor="#999999"/>
					<bottomPen lineWidth="0.25" lineColor="#999999"/>
					<rightPen lineWidth="0.25" lineColor="#999999"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font fontName="Verdana" size="7"/>
					<paragraph leftIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{PRODUCT}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="97fb07df-c468-41be-bd69-58f1d85ac90d" stretchType="RelativeToTallestObject" x="210" y="0" width="95" height="10"/>
				<box>
					<pen lineWidth="0.25" lineColor="#999999"/>
					<topPen lineWidth="0.25" lineColor="#999999"/>
					<leftPen lineWidth="0.25" lineColor="#999999"/>
					<bottomPen lineWidth="0.25" lineColor="#999999"/>
					<rightPen lineWidth="0.25" lineColor="#999999"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Verdana" size="7"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{SPOISUMA} == 0 ? "------" : new DecimalFormat("#,##0.00").format($F{SPOISUMA}) + " €"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="6a423b59-f3a4-4b80-8aea-0af81ccc44ef" stretchType="RelativeToTallestObject" x="305" y="0" width="115" height="10"/>
				<box>
					<pen lineWidth="0.25" lineColor="#999999"/>
					<topPen lineWidth="0.25" lineColor="#999999"/>
					<leftPen lineWidth="0.25" lineColor="#999999"/>
					<bottomPen lineWidth="0.25" lineColor="#999999"/>
					<rightPen lineWidth="0.25" lineColor="#999999"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Verdana" size="7"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{NPOISUMA} == 0 ? "------" : new DecimalFormat("#,##0.00").format($F{NPOISUMA}) + " €"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="760ba5d2-6e69-4e11-a925-9a9788b72604" stretchType="RelativeToTallestObject" x="420" y="0" width="55" height="10"/>
				<box>
					<pen lineWidth="0.25" lineColor="#999999"/>
					<topPen lineWidth="0.25" lineColor="#999999"/>
					<leftPen lineWidth="0.25" lineColor="#999999"/>
					<bottomPen lineWidth="0.25" lineColor="#999999"/>
					<rightPen lineWidth="0.25" lineColor="#999999"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Verdana" size="7"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[new DecimalFormat("#,##0.00").format($F{NOVEPOI}) + " €"]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
